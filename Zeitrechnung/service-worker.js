"use strict";

var CACHE_NAME = 'Zeit-v1.8.10';

self.addEventListener('install', event => {
    console.log('[install] Kicking off service worker registration!');
    event.waitUntil(
        caches.open(CACHE_NAME).then(cache => {
            return fetch('files-to-cache.json?' + CACHE_NAME).then(response => {
                return response.json();
            }).then(files => {
                console.log('[install] Adding files to cache from JSON: ', files);
                return cache.addAll(files);
            }).then(() => {
                console.log('[install] Adding file to cache: start.js');
                return cache.add('start.js');
            });
        }).then(() => {
            console.log('[cache] Created:', CACHE_NAME);
            return self.skipWaiting();
        })
    );
});

self.addEventListener('fetch', event => {
    var req = event.request;

    event.respondWith(
        new Promise((resolve, reject) => {
            if (req.url.endsWith('/start.js')) {
                console.log('[fetch] Trying Network first with a timeout of 2 sec: ', req.url);
                Promise.race([
                    fetch(req),
                    timeoutPromise(2000)
                ]).then(resolve).catch(e => caches.match(req).then(response => {
                    console.log('[fetch] Returning from Service Worker cache: ', req.url, e);
                    return response;
                }).then(resolve).catch(reject));
            } else {
                var idx = req.url.indexOf('time.html');
                if (idx !== -1) {
                    req = new Request(event.request.url.substr(0, idx + 9));
                }
                caches.match(req).then(response => {
                    if (response) {
                        console.log('[fetch] Returning from Service Worker cache: ', req.url);
                        return response;
                    }
                    console.log('[fetch] Returning from server: ', req.url);
                    return fetch(req);
                }).then(resolve).catch(reject)
            }
        })
    );
});

self.addEventListener('activate', event => {
    console.log('[activate] Activating service worker!');
    event.waitUntil(
        Promise.all([
            clients.claim().then(() => postMessageToAll({ t: 'install', reload: true })),
            caches.keys().then((keys) => Promise.all(keys.map(key => {
                if (key == CACHE_NAME)
                    return Promise.resolve("saved:" + key);
                if (!key.startsWith('Zeit-'))
                    return Promise.resolve("ignored:" + key);
                return caches.delete(key).then(() => Promise.resolve("killed:" + key));
            }))).then(keys => console.log("[cache] Cleaned up ", keys))
        ])
    );
});

self.addEventListener('push', evt => {
    console.log('[push] Incoming Push Message:', evt.timeStamp, evt.data);
    evt.waitUntil(postMessageToAll({ t: 'push', evtData: (evt.data ? evt.data.text() : null) }));
});

function postMessageToAll(data) {
    return clients.matchAll().then(clients => {
        return Promise.all(clients.map(client => {
            return client.postMessage(data);
        }));
    });
}

function timeoutPromise(timeout) {
    return new Promise((resolve, reject) => {
        setTimeout(reject, timeout);
    });
}
