var timer = null;
var informed = false;
var warnTime = 999999;

function calc() {
    var lineCount = $("#indata tr[id^='line']").length;

    try {
        var totalWorkTime = 0;
        var totalManuelBreakTime = 0;
        var pause = 0;
        var schule = 0;
        var gotBreak1 = false;
        var gotBreak2 = false;
        for (var i = 1; i <= lineCount; i++) {
            var diffKG = getDiff(i, i);
            // $('#diff-'+i).html(diffKG);
            totalWorkTime += diffKG;
            if (i > 1) {
                var type = jQuery.trim($('#type-' + i).val()).toUpperCase();
                var diffGK = getDiff(i, i - 1);
                if (type == 'D' || type == 'S') {
                    // $('#diff-'+i).append("+"+diffGK);
                    totalWorkTime += diffGK;
                    schule += diffGK;
                } else {
                    totalManuelBreakTime += diffGK;
                    // $('#diff-'+i).append("|"+diffGK);
                    if (!gotBreak1 &&
                        toMin($('#out-' + (i - 1)).val()) >= toMin("12:30") &&
                        toMin($('#in-' + i).val()) <= toMin("14:00") &&
                        diffGK >= 15) {
                        pause -= diffGK;
                        gotBreak1 = true;
                    }
                    if (!gotBreak2 &&
                        toMin($('#out-' + (i - 1)).val()) >= toMin("15:30") &&
                        diffGK >= 15) {
                        pause -= diffGK;
                        gotBreak2 = true;
                    }
                    if (!gotBreak1 && diffGK >= 15) {
                        pause -= diffGK;
                        gotBreak1 = true;
                    }
                    if (!gotBreak2 && diffGK >= 45) {
                        gotBreak2 = true;
                    }
                }
            }
        }
        if (totalWorkTime > 6 * 60 && (totalManuelBreakTime < 30 || !gotBreak1)) {
            pause += 30;
        }
        // FIXME: https://pedder1.dyndns.org/zeit-dev/?lines=3&auto=false&warn=normal&k0=07:30&g0=12:30&k1=12:50&g1=16:20&k2=16:35&g2=17:30
        if (totalWorkTime > 9 * 60 + pause) {
            pause += Math.min(15, totalWorkTime - (9 * 60 + pause));
        }
        if (totalWorkTime > 10 * 60 + pause) {
            pause += totalWorkTime - (10 * 60 + pause);
        }
        var sum = totalWorkTime;
        if (pause > 0 || totalManuelBreakTime > 0) {
            if (pause < 0) pause = 0;
            totalWorkTime -= pause;
            $('#pause').html(toText(toInt(pause / 60), pause % 60, true));
            $('#pause2').html(toText(toInt(totalManuelBreakTime / 60), totalManuelBreakTime % 60, true));
            $('#time-pause').show();
        } else {
            $('#pause').html("00:00");
            $('#pause2').html("00:00");
            $('#time-pause').hide();
        }
        if (schule != 0) {
            $('#schule').html(toText(toInt(schule / 60), schule % 60, true));
            $('#time-schule').show();
        } else {
            $('#schule').html("00:00");
            $('#time-schule').hide();
        }
        var extra = totalWorkTime - toMin("07:48");
        if (extra > 0) {
            $('#extra').html(toText(toInt(extra / 60), extra % 60, true));
            $('#time-extra').show();
        } else {
            $('#extra').html("00:00");
            $('#time-extra').hide();
        }
        $('#normal').html(toText(toInt(totalWorkTime / 60), totalWorkTime % 60, true));
        $('title').html(toText(toInt(totalWorkTime / 60), totalWorkTime % 60, true) + " Zeitrechnung");
        $('#sum').html(toText(toInt(sum / 60), sum % 60, true));

        if (totalWorkTime > toMin("07:48")) {
            $('#time-work-until').hide();
            $('#work-until').html("07:48");
        } else {
            $('#time-work-until').show();
            var in1 = toMin($('#in-1').val());
            var outN = toMin($('#out-' + lineCount).val());
            var sumPause = Math.max(pause + totalManuelBreakTime, 30);
            var workUntil = in1 + sumPause + toMin("07:48");
            var timeLeft = workUntil - outN;
            $('#work-until').html(toText(toInt(workUntil / 60), workUntil % 60, true) + " - " + toText(toInt(timeLeft / 60), timeLeft % 60, true));
        }

        if (!informed && totalWorkTime >= warnTime && totalWorkTime < warnTime + 15) {
            informed = true;
            var _warnTime = warnTime + 15;
            alertNow("Achtung\n\nIn weniger als 15 min sind es " + toText(toInt(_warnTime / 60), _warnTime % 60) + " Arbeitszeit!\nEs wird Zeit nach Hause zu gehen.");
        }
        if (informed && totalWorkTime < warnTime || totalWorkTime > warnTime + 15) {
            informed = false;
        }

    } catch (ex) {
        $('#b-stop').hide();
        $('#b-start').show();
        throw ex;
    }
}

function startCalc() {
    timer = null;
    var lineCount = $("#indata tr[id^='line']").length;
    var Zeit = new Date();
    $("#out-" + (lineCount)).val(toText(Zeit.getHours(), Zeit.getMinutes(), true));
    try {
        calc();
        timer = setTimeout(startCalc, 1000);
    } catch (ex) {}
}

function stopCalc() {
    if (timer != null) {
        clearTimeout(timer);
        timer = null;
    }
    informed = false;
}

function toInt(x) { return parseInt(parseFloat(x)); }

function getDiff(i, o) {
    var iIn = toMin($('#in-' + i).val());
    var iOut = toMin($('#out-' + o).val());
    return (i <= o) ? (iOut - iIn) : (iIn - iOut);
}

function toMin(t) {
    t = jQuery.trim(t);
    var s = t.split(":");
    return ((!isNaN(toInt(s[0]))) ? toInt(s[0]) : 0) * 60 + ((!isNaN(toInt(s[1]))) ? toInt(s[1]) : 0);
}

function toText(h, m, long) {
    var vz = "";
    if (m < 0) {
        h *= -1;
        m *= -1;
        vz = "-";
    }
    var min = (m < 10) ? "0" + m : m;
    var std = (long && h < 10) ? "0" + h : h;
    return vz + std + ":" + min;
}

function reCheckWarn() {
    var val = $('.warn-time:checked').val();
    if (val == "off") {
        warnTime = toMin("-1:00");
        informed = true;
    } else {
        warnTime = toMin(val);
        informed = false;
    }
}

function updateWarnTime() {
    informed = false;
    warnTime = $('#r-warn-off').attr('checked') ? 24 * 60 : $('#r-warn-normal').attr('checked') ? toMin("8:45") : toMin("5:45");
    // console.debug('[time] setting warnTime to ', warnTime);
}

function alertNow(msg) {
    if (!window.Notification) {
        console.log('[time] Notification not available');
        alert(msg);
    } else {
        if (window.AppNotify === 'granted') {
            Notification.requestPermission().then(function(result) {
                if (result === 'granted') {
                    var noti = new Notification("Zeitrechnung", {
                        body: msg,
                        icon: 'icon_big.png',
                        tag: 'time-for-home'
                    });
                } else {
                    alert(msg);
                }
            });
        } else if (window.AppNotify === 'default') {
            alert(msg);
        } else {
            console.log('[time] Notification not allowed', msg);
        }
    }
}

$(document).ready(function() {
    // $("#indata").delegate("input:not(.small)", {
    //     keyup: function(evt) {
    //         // console.log(evt);
    //         if (evt.shiftKey || evt.ctrlKey || evt.altKey) {
    //             return;
    //         }
    //         if (evt.keyCode == 8 ||
    //             evt.keyCode == 16 ||
    //             evt.keyCode == 17 ||
    //             evt.keyCode == 20 ||
    //             evt.keyCode == 9 ||
    //             (evt.keyCode >= 37 && evt.keyCode <= 40) ||
    //             evt.keyCode == 46) {
    //             return;
    //         }
    //         var t = $(this).val();

    //         if (/^[0-9]{5}$/.test(t)) {
    //             t = t.substr(0, 4);
    //         }

    //         if (/^[0-9]{3,4}$/.test(t)) {
    //             t = t.substr(0, 2) + ":" + t.substr(2);
    //             $(this).val(t);
    //         }

    //         if (!/^[0-9]{1,2}:?[0-9]{0,2}$/.test(t)) {
    //             $(this).val("");
    //         }
    //     },
    //     keypress: function(evt) {
    //         if (!evt.charCode) {
    //             return;
    //         }
    //         if (!(evt.charCode >= 48 && evt.charCode <= 57 || evt.charCode == 58)) {
    //             // Nur Ziffern sind erlaubt
    //             return false;
    //         }
    //     }
    // });
    $('#r-warn-off, #r-warn-normal, #r-warn-short').change(function() {
        updateWarnTime();
    });
    updateWarnTime();
    calc();

    if (window.Notification) {
        window.AppNotify = Notification.permission;
        if (Notification.permission !== 'denied') {
            $("#enable-notification, #disable-notification").removeClass("disabled");
            if (Notification.permission === 'granted') {
                $("#enable-notification").addClass("hidden");
                $("#disable-notification").removeClass("hidden");
            } else {
                $("#enable-notification").removeClass("hidden");
                $("#disable-notification").addClass("hidden");
            }
        }
    } else {
        window.AppNotify = 'default';
    }
    $("#enable-notification").click(function() {
        if (!window.Notification) {
            alert("Ihr Browser unterstützt Desktop Benachritigungen nicht.");
        } else if (Notification.permission === 'denied') {
            alert("Sie haben die Desktop Benachritigungen für diese Anwendung deaktiviert.");
        } else {
            Notification.requestPermission().then(function(res) {
                console.log(res);
                window.AppNotify = res;
                if (res === 'denied') {
                    $("#enable-notification, #disable-notification").addClass("disabled");
                } else if (res === 'granted') {
                    $("#enable-notification, #disable-notification").toggleClass("hidden").focus();
                    $("#enable-notification, #disable-notification").removeClass("disabled");
                }
            });
        }
    });
    $("#disable-notification").click(function() {
        window.AppNotify = 'default';
        $("#enable-notification, #disable-notification").toggleClass("hidden").focus();
    });
});
