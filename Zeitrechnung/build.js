var bez = {
    "in": "k",
    "out": "g",
    "type": "t"
};

function newLine(evt) {
    if (evt && evt.preventDefault) {
        evt.preventDefault();
    }
    var x = $('#indata tr').length;
    var tr = $('<tr></tr>').attr("id", "line-" + x).appendTo('#indata');
    var td1 = $('<td></td>').appendTo(tr);
    var td2 = $('<td></td>').appendTo(tr);
    var td3 = $('<td></td>').appendTo(tr);
    var td4 = $('<td></td>').appendTo(tr);
    $('<input />').attr({
        type: "text",
        length: "1",
        "class": "small",
        id: "type-" + x,
        maxlength: 1
    }).appendTo(td1);
    $('<input />').attr({
        type: "time",
        id: "in-" + x,
        maxlength: 5
    }).appendTo(td2);
    $('<input />').attr({
        type: "time",
        id: "out-" + x,
        maxlength: 5
    }).appendTo(td3);
    td4.attr("id", "diff-" + x);
    if (arguments[0] !== "donotupdate")
        updateSearch("lines", x);
    calc();
}

function delLine(evt) {
    if (evt && evt.preventDefault) {
        evt.preventDefault();
    }
    var lastLine = $('#indata tr:last');
    if (lastLine.attr("id") != "line-1") {
        lastLine.remove();
        if (arguments[0] !== "donotupdate") {
            var x = $('#indata tr').length;
            updateSearch("lines", x - 1);
        }
    }
    calc();
}

function startKlick(evt) {
    if (evt && evt.preventDefault) {
        evt.preventDefault();
    }
    startCalc();
    $('#b-start').hide();
    $('#b-stop').show().focus();
    if (arguments[0] !== "donotupdate")
        updateSearch("auto", "true");
}

function stopKlick(evt) {
    if (evt && evt.preventDefault) {
        evt.preventDefault();
    }
    stopCalc();
    $('#b-stop').hide();
    $('#b-start').show().focus();
    if (arguments[0] !== "donotupdate")
        updateSearch("auto", "false");
}

function init(evt) {
    if (evt && evt.preventDefault) {
        evt.preventDefault();
    }
    $("#indata input:not(.small)").attr("maxlength", 5);
    $("#indata input.small").attr("maxlength", 1);
    var td = $('<td></td>').appendTo('#indata tr:first');
    var img_p = $('<img />').attr({
        alt: "Zeile hinzufügen",
        width: 20,
        height: 20,
        src: "plus.png"
    });
    $('<a />').attr({
        style: "cursor: pointer;",
        title: "Zeile hinzufügen",
        href: "#"
    }).click(newLine).append(img_p).appendTo(td);
    td.append("&nbsp;");
    var img_m = $('<img />').attr({
        alt: "Zeile entfernen",
        width: 20,
        height: 20,
        src: "minus.png"
    });
    $('<a />').attr({
        style: "cursor: pointer;",
        title: "Zeile entfernen",
        href: "#"
    }).click(delLine).append(img_m).appendTo(td);

    $('#b-stop').hide();
    $('#time-extra').hide();
    $('#time-pause').hide();
    $('#time-schule').hide();

    // Automatische Warnung am Freitag auf 5:45 umstellen
    //var datum = new Date();
    //if (datum.getDay() == 5) {
    //	$('#r-warn-normal').removeAttr('checked');
    //	$('#r-warn-short').attr('checked', 'checked');
    //}

    $('#show_help').click(function (evt) {
        $('#help_frame').toggle();
        // console.log(evt);
        evt.preventDefault();
        evt.stopPropagation();
    }).css({
        cursor: 'pointer',
        borderRadius: '50%',
        display: 'inline-block',
        backgroundColor: '#fff',
        width: '1.1em',
        lineHeight: '1.1em',
        color: '#000',
        textAlign: 'center',
        fontFamily: 'Hack, monospace',
        '-ms-user-select': 'none',
        '-moz-user-select': 'none',
        '-webkit-user-select': 'none',
        'user-select': 'none',
        tabIndex: '0',
        fontSize: '0.5em',
        marginLeft: '2px'
    });

    prepareHistory(window);
    updateSearch();

    if (History.enabled) {
        $("body").on("change", "input[type=text],input[type=time]", null, function (evt) {
            var el = $(evt.target);
            var val = el.val();
            var id = el.attr("id");
            // console.log(id);
            var idp = id.split("-");
            updateSearch((bez[idp[0]] + (idp[1] - 1)), val);
            calc();
        });
        $("input[type=radio]").change(function (evt) {
            var el = $(evt.target);
            var id = el.attr("id");
            // console.log(id);
            var idp = id.split("-");
            updateSearch("warn", idp[2] == "short" ? "kurz" : idp[2]);
        });
    }

    $('#init').remove();
    $.get("env.php", function (data) {
        if (data.pub) {
            $('#corner-top-right').remove();
        }
    });
}

function prepareHistory(window, undefined) {
    var History = window.History;
    if (!History.enabled) {
        return false;
    }

    History.Adapter.bind(window, 'statechange', function () {
        var State = History.getState();
        History.log(State.data, State.title, State.url);
    });
}

function updateSearch(arg0, arg1) {
    if (!History.enabled && !inApp) {
        return;
    }

    var data = {};
    if (inApp) {
        try {
            data = JSON.parse(window.localStorage.getItem("data")) || data;
        } catch (e) {
            console.log(e);
        }
    } else {
        var oldSearch = location.search;
        oldSearch = (oldSearch != "" ? oldSearch.substring(1) : "");
        var parts = oldSearch.split("&");
        for (var i = 0; i < parts.length; i++) {
            var datum = parts[i].split("=");
            if (datum[0] !== undefined && datum[0] != "")
                data[datum[0]] = (datum[1] !== undefined ? datum[1] : "");
        }
    }
    if (arg0 !== undefined) {
        data[arg0] = arg1;
    }
    // url bauen
    var newSearch = "?";
    var nparts = [];
    nparts.push("lines=" + (data.lines = data.lines || 2));
    nparts.push("auto=" + (data.auto = data.auto || "false"));
    nparts.push("warn=" + (data.warn = data.warn || "normal"));
    for (var i = 0; i < data.lines; i++) {
        nparts.push("k" + i + "=" + (data["k" + i] = data["k" + i] || ""));
        nparts.push("g" + i + "=" + (data["g" + i] = data["g" + i] || ""));
        if (i > 0)
            nparts.push("t" + i + "=" + (data["t" + i] = data["t" + i] || ""));
    }
    newSearch += nparts.join("&");

    if (arg0 === undefined) {
        var x = $('#indata tr').length;
        var d = (x - 1) - data.lines;
        while (d < 0) {
            newLine("donotupdate");
            d++;
        }
        while (d > 0) {
            delLine("donotupdate");
            d--;
        }
        for (var i = 0; i < data.lines; i++) {
            $("input#in-" + (i + 1)).val(data["k" + i]);
            $("input#out-" + (i + 1)).val(data["g" + i]);
            if (i > 0)
                $("input#type-" + (i + 1)).val(data["t" + i]);
        }
        if (data.warn == "normal") {
            $('#r-warn-short').removeAttr('checked');
            $('#r-warn-off').removeAttr('checked');
            $('#r-warn-normal').attr('checked', 'checked');
        } else if (data.warn == "kurz") {
            $('#r-warn-normal').removeAttr('checked');
            $('#r-warn-off').removeAttr('checked');
            $('#r-warn-short').attr('checked', 'checked');
        } else if (data.warn == "off") {
            $('#r-warn-normal').removeAttr('checked');
            $('#r-warn-short').removeAttr('checked');
            $('#r-warn-off').attr('checked', 'checked');
        }

        if (data.auto == "true")
            startKlick("donotupdate");
        if (data.auto == "false")
            stopKlick("donotupdate");
    }

    if (History.enabled && !inApp) {
        History.replaceState(data, "Zeitrechnung", newSearch);
    }
    if (inApp) {
        window.localStorage.setItem("data", JSON.stringify(data));
    }
}

var inApp = false;
(function () {
    try {
        var s = window.location.search;
        // console.log(s);
        if (/\openAs=app/.test(s)) {
            inApp = true;
            var req = new Request('log.php?openAs=app', {
                method: "POST",
                cache: "no-cache",
                redirect: "follow",
                referrer: "time.html"
            });
            fetch(req).then(function (res) {
                console.info(req, res);
            }).catch(function (err) {
                console.error(req, err);
            });
        }
    } catch (err) {}
})();

$(document).ready(init());
