function _startServiceWorker(url) {
    navigator.serviceWorker.register(url, {
        scope: '.'
    }).then(function (registration) {
        console.log('[start] The service worker has been registered ', registration);
    });
}

function startServiceWorker(url) {
    if (!navigator.serviceWorker) {
        return; // We are on an IE or other old browser
    }

    navigator.serviceWorker.getRegistration().then(function (r) {
        console.log('[start] ServiceWorkerRegistation', r);
        if (r === undefined) {
            _startServiceWorker(url);
        } else {
            console.log('[start] Active Service Worker', r.active);
            if (r.active.scriptURL !== url) {
                _startServiceWorker(url);
            }
        }
    }).catch(function (e) {
        console.log(e);
    });

    navigator.serviceWorker.addEventListener('message', function (evt) {
        if (evt.data.t == 'install' && evt.data.reload) {
            $(document).ready(function () {
                $('#headline_div').css({ color: 'lime', cursor: 'pointer' }).attr('title', 'Update Available - Reload').click(function () {
                    location.reload();
                });
                console.log('[message] Update Availabe', evt);
            });
        } else {
            console.log('[message] Incoming Message', evt);
        }
    });
    console.log('[start] Message Listener Ready');
}

startServiceWorker('service-worker.js?v1.8.10');

// Unregister:
/*
navigator.serviceWorker.getRegistration()
  .then(function(a){return a ? a.unregister() : Promise.reject('No registerd service worker')})
  .then(function(b){console.log(b)})
  .catch(function(e){console.log(e)})
caches.keys()
  .then(function(keys){
      console.log(keys);
      return Promise.all(keys.map(function(key){caches.delete(key)}));
    })
  .then(function(x){console.log("Done " + x)})
  .catch(function(e){console.log(e)})
/* */
